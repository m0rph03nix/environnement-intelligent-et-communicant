#!/bin/bash

printf "\nStop containers if not already done : \n"
docker stop homeassistant  esphome  mosquitto_broker
printf "\nRemove containers : \n"
docker rm   homeassistant  esphome  mosquitto_broker